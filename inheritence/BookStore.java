package inheritence;

public class BookStore {
    
    public static void main(String args[]) {

        Book[] ba = new Book[5];
        ba[0] = new Book("How to obtain immortality", "God");
        ba[1] = new ElectronicBook("The ABCs to software engineering", "Joe Biden", 10);
        ba[2] = new Book("Existing in a modern world", "Shay Alex Lelichev");
        ba[3] = new ElectronicBook("Lets Make America Great Again", "Donald J. Trump", 2);
        ba[4] = new ElectronicBook("Mining Diamonds In Minecraft", "Bill Gates", 99);

        for (int i = 0; i < ba.length; i++) {
            System.out.println(ba[i] + "\n");
        }
    }
}
